# Automation priority: 1
# Test case importance: Low
# language: en
Feature: PetStoreConnection

	Scenario: PetStoreConnection
		Given I am in petStore HomePage
		When I sign in with login "j2ee" and password "j2ee"
		Then The welcome page is displayed
		And The user "ABC" is connected