package gherkinJava;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PetStoreConnexion {
	
	WebDriver driver;
	
	@Given("I am in petStore HomePage")
	public void i_am_in_petStore_HomePage() {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	    assertTrue(driver.findElement(By.xpath("//*[@id=\"MenuContent\"]/a[2]")).isDisplayed());
	}

	@When("I sign in with login {string} and password {string}")
	public void i_sign_in_with_login_and_password(String login, String password) {
		driver.findElement(By.xpath("//*[@id=\"MenuContent\"]/a[2]")).click();
		driver.findElement(By.name("username")).sendKeys(login);
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.name("signon")).click();
	}

	@Then("The welcome page is displayed")
	public void the_welcome_page_is_displayed() {

		assertTrue(driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"]")).isDisplayed());
		
	}

	@Then("The user {string} is connected")
	public void the_user_is_connected(String user) {
		String welcome = driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"]")).getText();
		assertEquals("Welcome " + user + "!" , welcome);
		driver.quit();
	}
}
